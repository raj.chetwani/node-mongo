const express = require('express')
const app = express()
const PORT = process.env.PORT || 5000

const connectDB = require('./config/db')

connectDB();

app.use(express.json({extended:false}))

//users api
app.use('/api/users',require('./routes/users/user'))

//orders api
app.use('/api/orders',require('./routes/orders/orders'))

//items api
app.use('/api/items',require('./routes/items/item'))

app.listen(PORT, () => {
    console.log(`Server running at port ${PORT}`);
})