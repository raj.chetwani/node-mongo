const express = require('express');
const router = express.Router();
const admin = require('../../middleware/admin');


router.get('/',require('../../controllers/items/getItem')())
router.post('/add',admin,require('../../controllers/items/addItem')())
router.delete('/delete/:id',admin,require('../../controllers/items/deleteItem')())
router.put('/update/:id',admin,require('../../controllers/items/updateItem')())


module.exports = router;