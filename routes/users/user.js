const express = require('express');
const router = express.Router();
const { check } = require('express-validator');


router.post('/login',[ check('email','Please enter valid email').isEmail(),
                        check('password','Password is required').exists()]
                        ,require('../../controllers/users/login')())


router.post('/register',[ check('name','Please enter a name').not().isEmpty(),
    check('email','Please enter valid email').isEmail(),
    check('password','Please enter password with 6 or more characters').isLength({ min: 6})]
    ,require('../../controllers/users/register')())


module.exports = router;