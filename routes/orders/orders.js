const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');


router.get('/myorders',auth,require('../../controllers/orders/getOrders')())
router.post('/create',auth,require('../../controllers/orders/createOrder')())
router.delete('/:id',auth,require('../../controllers/orders/deleteOrder')())
router.put('/:id',auth,require('../../controllers/orders/updateOrder')())


module.exports = router;