const jwt = require('jsonwebtoken');

const config = require('config');

module.exports = (req,res,next) => {
    const token = req.header('x-auth-token');

    if(!token){
        return res.status(401).json({ msg : 'No token,authorization denied'})
    }

    try {
        const decoded = jwt.verify(token,config.get('jwtSecret'));

        req.user = decoded.user;
        if(req.user.isAdmin === true){
            next();
        }else{
            return res.status(401).json({ msg : 'Authorization denied'})
        }
    } catch (error) {
        console.error(error);
        return res.status(401).json({ msg: "Token is invalid"});
    }
}