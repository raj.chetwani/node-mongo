const mongoose = require('mongoose');

const OrderSchema = mongoose.Schema({
    userID : {
        type: mongoose.Schema.Types.ObjectId,
        ref:'users'
    }, 
    itemList:[
        {
            type:mongoose.Schema.Types.ObjectId,
            ref:'items'
        }
    ],
    status: {
        type:String,
        default: "ordered"
    },
    date: {
        type : Date,
        default: new Date()
    }

})

module.exports = mongoose.model('orders',OrderSchema);