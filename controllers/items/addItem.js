const Items = require("../../models/Items");


module.exports = () => {
    return async (req,res) => {
        const { name,price } = req.body

        try {
            const newItem = new Items({
                name,
                price
            })
    
            const item = await newItem.save()
            res.json(item)
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server Error');
        }
    }
}