const Items = require("../../models/Items");

module.exports = () => {
    return async (req,res) => {
            const {name,price} = req.body
        
            const itemFields = {};
        
            if(name) itemFields.name = name;
            if(price) itemFields.price = price;
        
            try {
                let item = await Items.findById(req.params.id);
                if(!item) return res.status(404).json({ msg:"No item found"});
            
                item = await Items.findByIdAndUpdate(req.params.id,{$set: itemFields},{new:true})
                res.json(item);
            } catch (err) {
                console.error(err.message);
                res.status(500).send('Server error');
            }
        
        } 
    }