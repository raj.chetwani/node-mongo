const Items = require("../../models/Items");


module.exports = () => {
    return async (req,res) => {
            try {
                let item = await Items.findById(req.params.id);
                if(!item) return res.status(404).json({ msg:"No item found"});

                await Items.findByIdAndRemove(req.params.id);
                
                res.json({msg: "Item Deleted"});
            } catch (err) {
                console.error(err.message);
                res.status(500).send('Server error');
            }
        }
    }