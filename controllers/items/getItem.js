const Items = require("../../models/Items");


module.exports = () => {
    return async (req,res) => {
        try {
            const items = await Items.find()
            res.json(items)
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server Error');
        }
    }
}