const Orders = require('../../models/Orders')

module.exports = ( ) => {
    return async (req,res) => {
        const {itemList} = req.body

        const orderFields = {};
    
        if(itemList) orderFields.itemList = itemList;
    
        try {
            let order = await Orders.findById(req.params.id);
            if(!order) return res.status(404).json({ msg:"No order found"});
    
            if(order.userID.toString() !== req.user.id)
            return res.status(401).json({msg:"Not Authorized"});
    
            if(order.status === "delievered") return res.json({msg: "Order already delievered,cannot be modified now"})
    
    
            order = await Orders.findByIdAndUpdate(req.params.id,{$set: orderFields},{new:true})
            res.json(order);
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server error');
        }
    
    }
}