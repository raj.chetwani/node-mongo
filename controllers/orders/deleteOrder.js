const Orders = require('../../models/Orders')

module.exports = ( ) => {
    return async (req,res) => {
        try {
            let order = await Orders.findById(req.params.id);
            if(!order) return res.status(404).json({ msg:"No order found"});
            
            if(order.userID.toString() !== req.user.id)
            return res.status(401).json({msg:"Not Authorized"});
    
            await Orders.findByIdAndRemove(req.params.id);
            
            res.json({msg: "Order Deleted"});
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server error');
        }
    
    }
}