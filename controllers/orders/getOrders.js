const Orders = require('../../models/Orders')

module.exports = ( ) => {
    return async (req,res) => {
        try {
            const orders = await Orders.find({userID: req.user.id})
            res.json(orders)
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server Error');
        }
    
    }
}