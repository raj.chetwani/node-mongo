const Orders = require('../../models/Orders')

module.exports = ( ) => {
    return async (req,res) => {
        const  itemList  = req.body.itemList

    try {

        const newOrder = new Orders({
            itemList,
            userID : req.user.id
        })

        const order = await newOrder.save()

        res.json(order)

    }catch(err){
        console.error(err.message);
        res.status(500).send('Server error');
    }
    
    }
}