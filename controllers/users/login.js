const { validationResult } = require('express-validator');
const User = require('../../models/Users');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');


module.exports = () => {
    return async (req,res) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()){
            return res.status(400).json({ errors : errors.array()})
        }
    
        const { email,password } = req.body
    
        try {
    
            let user = await User.findOne({ email });
            if(!user){
                return res.status(400).json({ msg : "Invalid Credentials"});
            }

            const isMatch = await bcrypt.compare(password,user.password);

            if(!isMatch){
                return res.status(400).json({ msg : 'Invalid Credentials'});
            }

            const payload = {
                user:{
                    id: user.id
                }
            }
    
            jwt.sign(payload,config.get('jwtSecret'),{
                expiresIn : 360000
            },(err,token) =>{
                if(err) throw err;
                res.json({ token })
            })

        }catch(error){
            console.error(error.message);
            return res.status(500).send('server error');
        }
    }
}